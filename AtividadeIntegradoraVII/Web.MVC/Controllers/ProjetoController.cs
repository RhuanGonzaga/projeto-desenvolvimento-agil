﻿

using System.Data.Entity;
using Web.MVC.Models.Programador;

namespace Web.MVC.Controllers
{
    using Web.MVC.Filtros;
    using Dominio.Entidades;
    using Infra.Contexto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Bibliotecas.Enum;
    using Web.MVC.Models;
    using Web.MVC.Models.Projetos;

    [JsonAllowGet]
    public class ProjetoController : Controller
    {
        private readonly Contexto _contexto = new Contexto();
        // GET: Projeto
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Editar(int projetoId)
        {
            var editarProjetoModel = new EditarProjetoModel();
            var projeto = _contexto.Projetos.FirstOrDefault(l => l.ProjetoId == projetoId);

            editarProjetoModel.DadosProjeto = new DadosProjetoModel(projeto);

            var programadores = _contexto.Programadores
                .Select(l => new { l.ProgramadorId, l.Pessoa.Nome })
                .ToList();

             editarProjetoModel.Programadores = new List<SelectProgramadoresModel>();
            ; foreach (var programador in programadores)
            {
                var programadorModel = new SelectProgramadoresModel()
                {
                    Nome = programador.Nome,
                    ProgramadorId = programador.ProgramadorId
                };
                editarProjetoModel.Programadores.Add(programadorModel);
            }

            return View(editarProjetoModel);
        }
        public ActionResult PostEditar(DadosProjetoModel dadosProjetoModel)
        {
            try
            {
                var projeto = _contexto.Projetos
                    .FirstOrDefault(l => l.ProjetoId == dadosProjetoModel.ProjetoId);

                if (projeto == null) return Json(true);

                projeto.Status = dadosProjetoModel.Status;
                projeto.Titulo = dadosProjetoModel.Titulo;
                projeto.DataFim = dadosProjetoModel.DataFim;
                projeto.DataInicio = dadosProjetoModel.DataInicio;
                projeto.Descricao = dadosProjetoModel.Descricao;

                _contexto.Entry(projeto).State = EntityState.Modified;
                _contexto.SaveChanges();

                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
           
        }

        public ActionResult Cadastrar()
        {
            var programadores = _contexto.Programadores
                .Select(l => new {l.ProgramadorId, l.Pessoa.Nome})
                .ToList();

            var programadoresModel = new List<SelectProgramadoresModel>();
;           foreach (var programador in programadores)
            {
                var programadorModel = new SelectProgramadoresModel()
                {
                    Nome = programador.Nome,
                    ProgramadorId = programador.ProgramadorId
                };
                programadoresModel.Add(programadorModel);
            }
            return View(programadoresModel);
        }

        public JsonResult Excluir(int projetoId)
        {
            try
            {
                var projeto = _contexto.Projetos.FirstOrDefault(l => l.ProjetoId == projetoId);

                if (projeto == null) return Json(false);

                _contexto.Projetos.Remove(projeto);
                _contexto.SaveChanges();

                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
                throw;
            }
        }

        public JsonResult ConcluirProjeto(int projetoId)
        {
            try
            {
                var projeto = _contexto.Projetos.FirstOrDefault(l => l.ProjetoId == projetoId);

                if (projeto == null) return Json(false);

                projeto.Status = StatusProjeto.Concluido.Status;


                _contexto.Entry(projeto).State = EntityState.Modified;
                _contexto.SaveChanges();

                return Json(true);
            }
            catch (Exception)
            {
                Console.WriteLine();
                throw;
            }
        }

        public JsonResult PausarProjeto(int projetoId)
        {
            try
            {
                var projeto = _contexto.Projetos.FirstOrDefault(l => l.ProjetoId == projetoId);

                if (projeto == null) return Json(false);

                projeto.Status = StatusProjeto.Pausado.Status;


                _contexto.Entry(projeto).State = EntityState.Modified;
                _contexto.SaveChanges();

                return Json(true);
            }
            catch (Exception)
            {
                Console.WriteLine();
                throw;
            }
        }

        public JsonResult IniciarProjeto(int projetoId)
        {
            try
            {
                var projeto = _contexto.Projetos.FirstOrDefault(l => l.ProjetoId == projetoId);

                if (projeto == null) return Json(false);

                projeto.Status = StatusProjeto.Andamento.Status;

                
                _contexto.Entry(projeto).State = EntityState.Modified;
                _contexto.SaveChanges();

                return Json(true);
            }
            catch (Exception )
            {
                Console.WriteLine();
                throw;
            }

        }

        public JsonResult PostCadastrar(DadosProjetoModel dadosProjetoModel)
        {
            try
            {
                var projeto = new Projeto
                {
                    Titulo = dadosProjetoModel.Titulo,
                    DataInicio = dadosProjetoModel.DataInicio,
                    DataFim = dadosProjetoModel.DataFim,
                    Descricao = dadosProjetoModel.Descricao,
                    Status = StatusProjeto.NaoIniciado.Status
                };

                _contexto.Projetos.Add(projeto);
                _contexto.SaveChanges();

                var sessao = (SessionModel) Session["Sessao"];

                var gerente = _contexto.Gerentes
                    .FirstOrDefault(x => x.PessoaId == sessao.Pessoa.PessoaId);

                if (gerente == null) return Json(false);

                foreach (int programadorId in dadosProjetoModel.ProgramadoresId)
                {
                    var programadorProjeto = new ProgramadorProjeto
                    {
                        ProjetoId = projeto.ProjetoId,
                        GerenteId = gerente.GerenteId,
                        ProgramadorId = programadorId
                    };
                    _contexto.ProgramadorProjeto.Add(programadorProjeto);
                    _contexto.SaveChanges();
                }
                


                return Json(new
                {
                    redirectUrl = Url.Action($"Index", $"PainelDeControle"),
                    isRedirect = true
                });
            }
            catch (Exception er )
            {
                return Json(false);
            }
        }

        public ActionResult Validar()
        {
           
            return View();
        }
    }
}