﻿using Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.MVC.Models
{
    using System.Web;
    using Dominio.Entidades;
    public class PainelDeControleModel
    {
        public Projeto Projeto { get; set; }

        public static bool IsGerente { get; set; }

        public List<Dominio.Entidades.Programador> Programadores { get; set; } = new List<Dominio.Entidades.Programador>();

        public Gerente Gerente { get; set; }

        public PainelDeControleModel()
        {
            
        }
    }
}