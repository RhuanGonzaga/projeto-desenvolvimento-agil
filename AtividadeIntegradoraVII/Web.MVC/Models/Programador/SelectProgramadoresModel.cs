﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.MVC.Models.Programador
{
    public class SelectProgramadoresModel
    {
        public int ProgramadorId { get; set; }

        public string Nome { get; set; }
    }
}