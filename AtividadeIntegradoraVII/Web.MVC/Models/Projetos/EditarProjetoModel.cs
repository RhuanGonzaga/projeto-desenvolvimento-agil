﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dominio.Entidades;
using Web.MVC.Models.Programador;

namespace Web.MVC.Models.Projetos
{
    public class EditarProjetoModel
    {
        public DadosProjetoModel DadosProjeto { get; set; }

        public List<SelectProgramadoresModel> Programadores { get; set; }

        public EditarProjetoModel()
        {

        }

    }
}