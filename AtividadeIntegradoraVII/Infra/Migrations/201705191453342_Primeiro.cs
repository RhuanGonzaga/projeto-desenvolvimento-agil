namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Primeiro : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Gerente",
                c => new
                    {
                        GerenteId = c.Int(nullable: false, identity: true),
                        PessoaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GerenteId)
                .ForeignKey("dbo.Pessoa", t => t.PessoaId, cascadeDelete: true)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.Pessoa",
                c => new
                    {
                        PessoaId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        Cpf = c.String(),
                        Senha = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.PessoaId);
            
            CreateTable(
                "dbo.ProgramadorProjeto",
                c => new
                    {
                        ProgramadorProjetoId = c.Int(nullable: false, identity: true),
                        ProjetoId = c.Int(nullable: false),
                        GerenteId = c.Int(nullable: false),
                        ProgramadorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProgramadorProjetoId)
                .ForeignKey("dbo.Gerente", t => t.GerenteId, cascadeDelete: true)
                .ForeignKey("dbo.Programador", t => t.ProgramadorId, cascadeDelete: true)
                .ForeignKey("dbo.Projeto", t => t.ProjetoId, cascadeDelete: true)
                .Index(t => t.ProjetoId)
                .Index(t => t.GerenteId)
                .Index(t => t.ProgramadorId);
            
            CreateTable(
                "dbo.Programador",
                c => new
                    {
                        ProgramadorId = c.Int(nullable: false, identity: true),
                        PessoaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProgramadorId)
                .ForeignKey("dbo.Pessoa", t => t.PessoaId, cascadeDelete: false)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.Projeto",
                c => new
                    {
                        ProjetoId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        Titulo = c.String(),
                    })
                .PrimaryKey(t => t.ProjetoId);
            
            CreateTable(
                "dbo.Mensagem",
                c => new
                    {
                        MensagemId = c.Int(nullable: false, identity: true),
                        Titulo = c.String(),
                        Conteudo = c.String(),
                        Assunto = c.String(),
                    })
                .PrimaryKey(t => t.MensagemId);
            
            CreateTable(
                "dbo.Tarefa",
                c => new
                    {
                        TarefaId = c.Int(nullable: false, identity: true),
                        Titulo = c.String(),
                        DataFinal = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.TarefaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProgramadorProjeto", "ProjetoId", "dbo.Projeto");
            DropForeignKey("dbo.ProgramadorProjeto", "ProgramadorId", "dbo.Programador");
            DropForeignKey("dbo.Programador", "PessoaId", "dbo.Pessoa");
            DropForeignKey("dbo.ProgramadorProjeto", "GerenteId", "dbo.Gerente");
            DropForeignKey("dbo.Gerente", "PessoaId", "dbo.Pessoa");
            DropIndex("dbo.Programador", new[] { "PessoaId" });
            DropIndex("dbo.ProgramadorProjeto", new[] { "ProgramadorId" });
            DropIndex("dbo.ProgramadorProjeto", new[] { "GerenteId" });
            DropIndex("dbo.ProgramadorProjeto", new[] { "ProjetoId" });
            DropIndex("dbo.Gerente", new[] { "PessoaId" });
            DropTable("dbo.Tarefa");
            DropTable("dbo.Mensagem");
            DropTable("dbo.Projeto");
            DropTable("dbo.Programador");
            DropTable("dbo.ProgramadorProjeto");
            DropTable("dbo.Pessoa");
            DropTable("dbo.Gerente");
        }
    }
}
