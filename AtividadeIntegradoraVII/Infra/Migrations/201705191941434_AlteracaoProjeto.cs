namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteracaoProjeto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projeto", "DataInicio", c => c.DateTime(nullable: false));
            AddColumn("dbo.Projeto", "DataFim", c => c.DateTime(nullable: false));
            AddColumn("dbo.Projeto", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projeto", "Status");
            DropColumn("dbo.Projeto", "DataFim");
            DropColumn("dbo.Projeto", "DataInicio");
        }
    }
}
