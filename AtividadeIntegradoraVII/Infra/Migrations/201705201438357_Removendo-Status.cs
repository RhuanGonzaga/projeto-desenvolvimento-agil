namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovendoStatus : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Projeto", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projeto", "Status", c => c.Boolean(nullable: false));
        }
    }
}
