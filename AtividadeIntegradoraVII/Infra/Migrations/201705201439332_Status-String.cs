namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StatusString : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projeto", "Status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projeto", "Status");
        }
    }
}
